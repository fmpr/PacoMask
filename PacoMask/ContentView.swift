//
//  ContentView.swift
//  PacoMask
//  Basic Augmented reality APP that display an darth vader mask rcproject
//
//  Created by Francisco Martin on 29/05/23.
//

import SwiftUI
import RealityKit
import ARKit

struct ContentView : View {
    var body: some View {
        ARViewContainer().edgesIgnoringSafeArea(.all)
    }
}

struct ARViewContainer: UIViewRepresentable {
    
    func makeUIView(context: Context) -> ARView {
        
        let arView = ARView(frame: .zero)
        
        // Load the "Box" scene from the "Experience" Reality File
        
        //let boxAnchor = try! Experience.loadBox()
        let faceScene = try! DarthVader.loadScene()
        arView.scene.anchors.append(faceScene)
        
        // Add the box anchor to the scene
        
        //arView.scene.anchors.append(boxAnchor)
        let arConfig = ARFaceTrackingConfiguration()
        arView.session.run(arConfig)
        
        return arView
        
    }
    
    func updateUIView(_ uiView: ARView, context: Context) {}
    
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
